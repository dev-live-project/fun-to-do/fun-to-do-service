package com.devProject.crud.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import com.devProject.crud.dao.TaskDao;
import com.devProject.crud.entity.Task;

@Service
public class TaskService {

	@Autowired
	private TaskDao taskDao;
	
	public Task saveTask(Task task) {
		return taskDao.save(task);
	}
	
	public List<Task> getTasks() {
		List<Task> tasks = new ArrayList<>();
		taskDao.findAll().forEach(tasks::add);
		return tasks;
	}
	
	public Task getTask(Integer taskId) {
		return taskDao.findById(taskId).orElseThrow();
	}
	
	public void deleteTask(Integer taskId) {
		taskDao.deleteById(taskId);
	}
	
	public Task updateTask(Task updatedTask) {
//		taskDao.findById(task.getTaskId()).orElseThrow();
//		return taskDao.save(task);
//		
		
		// Retrieve the existing task from the database
	    Task existingTask = taskDao.findById(updatedTask.getTaskId())
	                                .orElseThrow();

	    // Update only the desired fields
	    existingTask.setDescription(updatedTask.getDescription());
	    existingTask.setCompleted(updatedTask.getCompleted());
	    existingTask.setImportant(updatedTask.getImportant());
	    existingTask.setUpdated_dt(new Date());
	    // Update other fields as needed

	    // Save the updated task
	    return taskDao.save(existingTask);
	}
}
