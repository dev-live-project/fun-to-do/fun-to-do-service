package com.devProject.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devProject.crud.entity.Task;
import com.devProject.crud.service.TaskService;

@RestController
@CrossOrigin("http://localhost:4200")
public class TaskController {

	@Autowired
	private TaskService taskService;
	
	@PostMapping("/save/task")
	public Task saveTask(@RequestBody Task task) {
		return taskService.saveTask(task);
	}
	
	
	@GetMapping("/get/task")
	public List<Task> getTasks(){
		return taskService.getTasks();
	}
	
	@GetMapping("/get/task/{taskId}")
	public Task getTask(@PathVariable Integer taskId) {
		return taskService.getTask(taskId);
	}
	
	@DeleteMapping("/delete/task/{taskId}")
    public void deleteTask(@PathVariable Integer taskId) {
        taskService.deleteTask(taskId);
    }

    @PutMapping("/update/task")
    public Task updateTask(@RequestBody Task task) {
        return taskService.updateTask(task);
        
        
        
    }
	
}
