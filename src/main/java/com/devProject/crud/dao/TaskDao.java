package com.devProject.crud.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.devProject.crud.entity.Task;

@Repository
public interface TaskDao extends CrudRepository<Task, Integer> {

}
